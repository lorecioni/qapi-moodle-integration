package main;

import java.text.DecimalFormat;
import java.util.ArrayList;

/*
 * @Author: Lorenzo Cioni
 */

public class Main {
	public static void main(String [] args) {
		System.out.println("Moodle course fetcher started");

		double startTime = System.currentTimeMillis(); 
		Runtime runtime = Runtime.getRuntime();
		
		CourseManager cm = new CourseManager();
		
		if(args.length == 0){			
			cm.run();
		} else {
			ArrayList<Integer> courses_id = new ArrayList<Integer>();
			for(int i = 0; i < args.length; i++){
				courses_id.add(Integer.parseInt((args[i])));
			}
			cm.run(courses_id);
		}
	
		usedTimeMemory(startTime, runtime);
	}
	
	public static void usedTimeMemory(double startTime, Runtime rt){
		
		float mb = (1024*1024);
		//Print used memory
		DecimalFormat df = new DecimalFormat();
		df.setMinimumFractionDigits(3);
        System.out.println("\nUsed Memory: "
            + df.format((rt.totalMemory() - rt.freeMemory())/mb) + " Mb");
        
        //Print computation time
        double endTime = System.currentTimeMillis(); 
        
        double time = (endTime - startTime)/1000;
        System.out.println("Time: " + df.format(time) + " s");
	}
}
