package main;

import java.util.Date;

public class Course {

	private int course_id;
	private Date last_modified;
	private Date processed;
	private Date indexed;
	private String xml;
	private String AXRQID;

	public Course() {
	}

	public Course(int id, Date last_modified, Date processed, Date indexed,
			String xml) {
		this.course_id = id;
		this.last_modified = last_modified;
		this.processed = processed;
		this.indexed = indexed;
		this.xml = xml;
	}

	public int getCourse_id() {
		return course_id;
	}

	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}

	public Date getLast_modified() {
		return last_modified;
	}

	public void setLast_modified(Date last_modified) {
		this.last_modified = last_modified;
	}

	public Date getProcessed() {
		return processed;
	}

	public void setProcessed(Date processed) {
		this.processed = processed;
	}

	public Date getIndexed() {
		return indexed;
	}

	public void setIndexed(Date indexed) {
		this.indexed = indexed;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public String getAXRQID() {
		return AXRQID;
	}

	public void setAXRQID(String aXRQID) {
		AXRQID = aXRQID;
	}

}
