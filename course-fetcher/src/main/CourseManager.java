package main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.File;
import java.net.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import org.xml.sax.*;
import javax.xml.xpath.*;
import java.io.StringReader;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import clientSOAP.AXCPclient;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.text.*;

public class CourseManager {

	private ArrayList<Integer> courses_id;
	private ArrayList<Course> courses;
	private Properties props;

	public CourseManager() {
		this.courses_id = new ArrayList<Integer>();
		this.courses = new ArrayList<Course>();

		// Load configuration file
		this.props = new Properties();
		try {
			props.load(this.getClass().getResourceAsStream("/config.properties"));
		} catch (FileNotFoundException e) {
			System.err.println(e);
		} catch (IOException e) {
			System.err.println(e);
		}
	}

	// Main method
	public void run() {
		getCoursesID();
		getCourses();
		removeOldCourses();
		System.out.println("\nDONE! Data table update");
	}
	
	//Main method for a single course
	public void run(ArrayList<Integer> course_id){
		courses_id = course_id;
		getCourses();
		System.out.println("\nDONE! Data table update");
	}

	private void getCoursesID() {
		String url = props.getProperty("COURSE_ID_URL");
		try {
			String response = send(url);
			
			if(!response.substring(0,1).matches("[0-9]")){
				response = response.substring(1);
			}
			
			// Store courses' id in an array
			String[] ids = response.split(",");
			for (String id : ids) {
				int course_id = Integer.parseInt(id.trim());
				courses_id.add(course_id);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e);
			error();
		}
	}

	private void getCourses() {
		// For each courses, generate xml and updates table
		Iterator<Integer> it = courses_id.iterator();
		while (it.hasNext()) {
			Integer id = it.next();
			String url = props.getProperty("COURSE_XML_URL") + id;
			try {
				String response = send(url);
				if(!response.substring(0,1).equals("<")){
					response = response.substring(1);
				}
				String last_modified = parseXML(response, "last_modified");
				if (storeCourse(id, response, last_modified) == false) {
					System.out.println("ERROR: Course id " + id + " not saved");
					error();
				}
			} catch (Exception e) {
				System.err.println(e);
				error();
			}
		}
	}

	private boolean storeCourse(int id, String xml, String last_modified) {

		Connection cc = null;
		String query = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("MySQL JDBC Driver missing");
			return false;
		}

		// Insert the new courses in the table and update
		try {

			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd hh:mm:ss");
			Date parsedDate;
			parsedDate = dateFormat.parse(last_modified);
			Timestamp modified = new Timestamp(parsedDate.getTime());

			try {
				cc = DriverManager.getConnection(props.getProperty("DB_URL"),
						props.getProperty("DB_USER"),
						props.getProperty("DB_PASS"));
				Statement ss = (Statement) cc.createStatement();

				query = "SELECT * FROM " + props.getProperty("DB_TABLE") + " WHERE course_id = " + id;

				ResultSet rs = ss.executeQuery(query);

				if (!rs.isBeforeFirst()) {
					// Course is not in table
					System.out.println("Course id " + id + " not found");
					Date date = new Date();
					Course c = new Course(id, modified, new Timestamp(date.getTime()), null, xml);
					courses.add(c);
					// Uploading course
					String AXRQID = upload(c, true);
					if (AXRQID != null) {
						query = "INSERT INTO "
								+ props.getProperty("DB_TABLE")
								+ " (course_id, indexed, last_modified, processed, AXRQID, AXOID,  status) VALUES ("
								+ id + ", null , '" + modified + "','"
								+ c.getProcessed() + "','" + AXRQID + "', 'null', 'pending')";
						// If course uploading routine ended correctly, store
						// the course in database
						ss.close();
						ss = (Statement) cc.createStatement();
						ss.executeUpdate(query);
						return true;
					} else {
						query = "INSERT INTO "
								+ props.getProperty("DB_TABLE")
								+ " (course_id, last_modified, processed, indexed, message, status, AXOID) VALUES ("
								+ id + ", '" + modified + "','"
								+ c.getProcessed() + "', null ,'Connection failure', 'error', 'null')";
						// If course uploading routine ended correctly, store
						// the course in database
						ss.close();
						ss = (Statement) cc.createStatement();
						ss.executeUpdate(query);
						return false;
					}
				} else { // Course is in table, check if it has been modified
							// since last time
					try {
						while (rs.next()) {
							String last_mod = rs.getString("last_modified").toString();
							DateFormat dateForm = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
							Date date = dateForm.parse(last_mod);
							long stored_time = date.getTime();
							date = dateForm.parse(last_modified);
							long last_time = date.getTime();
							
							String status = rs.getString("status").toString();
							
							if (last_time == stored_time && status.equals("indexed")) {
								System.out.println("Course id " + id + " not changed");
								return true;
							} else if (status.equals("pending")) {
								System.out.println("Course id " + id + " already processed");
								return false;
							}else {
								System.out.println("Course id " + id + " changed. Updating table...");
								date = new Date();
								Course c = new Course(id, modified,new Timestamp(date.getTime()), null, xml);
								// Uploading course
								String AXRQID = upload(c, false);
								if (AXRQID != null) {
									query = "UPDATE " + props.getProperty("DB_TABLE")
											+ " SET last_modified = '" + modified
											+ "', processed = '" + c.getProcessed()
											+ "', AXRQID = '" + AXRQID
											+ "', indexed = null, AXOID = null, message = null, status = 'pending' WHERE course_id = " + id;
									ss.close();
									ss = (Statement) cc.createStatement();
									ss.executeUpdate(query);
									return true;
								} else {
									query = "UPDATE "
											+ props.getProperty("DB_TABLE")
											+ " SET last_modified = '" + modified
											+ "', processed = '" + c.getProcessed()
											+ "', AXRQID = 'null', indexed = null, AXOID = null, message = 'Connection failure' WHERE course_id = " + id;
									ss.close();
									ss = (Statement) cc.createStatement();
									ss.executeUpdate(query);
									return false;
								}
							}
						}

					} catch (ParseException e) {
						System.err.println(e);
						return false;
					}
				}
			} catch (SQLException e) {
				System.err.println(e);
				return false;
			}

			finally {
				try {
					cc.close();
				} catch (SQLException e) {
					System.err.println(e);
				}
			}

		} catch (ParseException e) {
			System.err.println(e);
		}
		return false;
	}

	private void removeOldCourses() {
		// Remove past courses from the table
		Connection cc = null;

		try {
			String query = null;

			cc = DriverManager.getConnection(props.getProperty("DB_URL"),
					props.getProperty("DB_USER"), props.getProperty("DB_PASS"));
			Statement ss = (Statement) cc.createStatement();

			String set = "(";
			Iterator<Integer> it = courses_id.iterator();
			while (it.hasNext()) {
				set += it.next().toString() + ",";
			}
			set += "0)";

			query = "SELECT * FROM " + props.getProperty("DB_TABLE") + " WHERE course_id NOT IN " + set;

			ResultSet rs = ss.executeQuery(query);

			while (rs.next()) {
				Statement st = (Statement) cc.createStatement();
				System.out.println("Course id " + rs.getString("course_id") + " removed");
				long id = Long.parseLong(rs.getString("course_id"));
				Course c = getCourseByID(id);
				removeIndex(c);

				query = "DELETE FROM " + props.getProperty("DB_TABLE") + " WHERE course_id = " + id;

				st.executeUpdate(query);
				removeCourseByID(id);
			}

		} catch (SQLException e) {
			System.err.println(e);
			return;
		}

		finally {
			try {
				cc.close();
			} catch (SQLException e) {
				System.err.println(e);
			}
		}
	}

	// Upload course to Drupal and start indexing
	private String upload(Course c, boolean upload) {
		System.out.println("Course uploading...");
		String arguments = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		Date date = new Date();
		String folder = "moodle_" + c.getCourse_id() + "_" + dateFormat.format(date);
		if (createDir(folder) == true) {
			String path = props.getProperty("UPLOAD_DIR_PATH") + folder + "/";
			if (createMetadataXML(path, c.getXml()) != null) {
				if (storeXML(path, c.getXml()) == true) {
					if ((arguments = createRequest(path, folder, c.getXml(), upload)) != null) {
						AXCPclient client = new AXCPclient(arguments, folder);
						if (client.send() == true) {
							System.out.println("SUCCESS: Upload of course id " + c.getCourse_id() + " started");
							return folder;
						} else {
							System.err.println("ERROR: Upload of course id " + c.getCourse_id() + " failed");
							return null;
						}
					}
				}
			}
		}
		return null;
	}

	private boolean removeIndex(Course c) {
		// Remove a course from index
		System.out.println("Removing course from index...");
		return false;
	}

	private Course getCourseByID(long id) {
		Iterator<Course> it = courses.iterator();
		while (it.hasNext()) {
			Course c = it.next();
			if (c.getCourse_id() == id) {
				return c;
			}
		}
		return null;
	}

	private boolean removeCourseByID(long id) {
		Iterator<Course> it = courses.iterator();
		while (it.hasNext()) {
			Course c = it.next();
			if (c.getCourse_id() == id) {
				courses.remove(c);
				return true;
			}
		}
		return false;
	}

	private String send(String url) throws Exception {
		String USER_AGENT = "Mozilla/5.0";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		// GET method
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", USER_AGENT);
		System.out.println("Sending request to URL : " + url);
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer res = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			res.append(inputLine);
		}
		in.close();

		String response = res.toString();
		return response;
	}

	private String parseXML(String xml, String field) {
		InputSource source = new InputSource(new StringReader(xml));
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();

			Document document = db.parse(source);

			XPathFactory xpathFactory = XPathFactory.newInstance();
			XPath xpath = xpathFactory.newXPath();

			String result = xpath.evaluate("/course/" + field, document);
			return result;

		} catch (ParserConfigurationException e) {
			System.err.println(e);
		} catch (XPathExpressionException e) {
			System.err.println(e);
		} catch (SAXException e) {
			System.err.println(e);
		} catch (IOException e) {
			System.err.println(e);
		}

		return null;
	}

	private boolean createDir(String folder) {
		String path = props.getProperty("UPLOAD_DIR_PATH") + folder;
		File dir = new File(path);
		if (dir.exists()) {
			return true;
		}
		boolean success = (dir).mkdir();

		if (success) {
			System.out.println("Created: " + path);
		} else {
			System.out.println("ERROR creating: " + path);
			return false;
		}
		return true;
	}

	// Create and prepare metadata file
	private String createMetadataXML(String path, String xml) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		Date date = new Date();
		try {
			File file = new File(path + "metadata.xml");

			if (file.exists()) {
				System.out.println("File metadata.xml already exists");
				return null;
			} else if (file.createNewFile()) {
				String result = "";
				System.out.println("File metadata.xml created");
				PrintWriter writer = new PrintWriter(path + "metadata.xml",
						"UTF-8");
				result += "<?xml version='1.0' encoding='UTF-8' standalone='no' ?>";
				result += "<metadata xmlns='http://www.openarchives.org/OAI/2.0/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>";
				result += "<eclap:ECLAPObjectWrap xmlns:dc='http://purl.org/dc/elements/1.1/' xmlns:eclap='http://www.eclap.eu/ECLAPSchemaV0' xmlns:xml='http://www.w3.org/XML/1998/namespace'>";
				result += "<eclap:ECLAPObject><eclap:DC>";
				result += "<dc:creator>" + parseXML(xml, "course_creator/userid")
						+ "</dc:creator><dc:date>" + dateFormat.format(date)
						+ "</dc:date>";
				result += "<dc:description xml:lang='en'>"
						+ parseXML(xml, "description") + "</dc:description>";
				result += "<dc:title xml:lang='en'>" + parseXML(xml, "title")
						+ "</dc:title>";
				result += "<dc:type xml:lang='en'>course</dc:type>";
				result += "<dc:language xml:lang='en'>en</dc:language>";
				result += "</eclap:DC><eclap:TechnicalMetadata>";
				result += "<eclap:course_id>" + parseXML(xml, "id")
						+ "</eclap:course_id>";
				result += "</eclap:TechnicalMetadata></eclap:ECLAPObject></eclap:ECLAPObjectWrap></metadata>";
				writer.print(result);
				writer.close();
				return result;
			} else {
				System.out.println("File metadata.xml error");
				return null;
			}
		} catch (IOException e) {
			System.err.println(e);
		}
		return null;
	}

	private boolean storeXML(String path, String xml) {
		try {
			// Change file extension to be recognized
			File file = new File(path + "course.xm");

			if (file.exists()) {
				System.out.println("File course.xm already exists");
				return false;
			} else if (file.createNewFile()) {
				System.out.println("File course.xm created");
				PrintWriter writer = new PrintWriter(path + "course.xm",
						"UTF-8");
				writer.println(xml);
				writer.close();
				return true;
			} else {
				System.out.println("File course.xm error");
				return false;
			}
		} catch (IOException e) {
			System.err.println(e);
		}
		return false;
	}

	// Create request parameters for upload
	private String createRequest(String path, String folder, String xml,boolean upload) {
		try {
			// Change file extension to be recognized
			File file = new File(path + "request.req");

			if (file.exists()) {
				System.out.println("File request.req already exists");
				return null;
			} else if (file.createNewFile()) {
				System.out.println("File request.req created");
				String op = "Upload";
				if (upload == false) {
					op = "Update";
				}
				PrintWriter writer = new PrintWriter(path + "request.req",
						"UTF-8");
				String result = "";
				result += "<Definition><AXCP_Rule><Arguments>";
				result += "<Parameter Name='fileUrl' Type='String'>{'url': '"+ props.getProperty("UPLOAD_DIR_URL").replace("\\", "\\\\") + folder + "\\\\course.xm'" + ",'filename':'course.xm','mime':'application/octet-stream','mode':'file','localFolder':'" + folder + "'}</Parameter>";
				result += "<Parameter Name='validate' Type='Boolean'>false</Parameter>";
				result += "<Parameter Name='owner' Type='String'>" + parseXML(xml, "course_creator/userid") + ";" + parseXML(xml, "course_creator/affiliation") + "</Parameter>";
				result += "<Parameter Name='protectObject' Type='Boolean'>false</Parameter>";
				result += "<Parameter Name='noHttp' Type='Boolean'>false</Parameter>";
				result += "<Parameter Name='noP2p' Type='Boolean'>true</Parameter>";
				result += "<Parameter Name='drupalId' Type='String'></Parameter>";
				result += "<Parameter Name='metadataUrl' Type='String'>"
						+ props.getProperty("UPLOAD_DIR_URL") + folder + "\\metadata.xml"
						+ "</Parameter>";
				result += "<Parameter Name='classification' Type='String'></Parameter>";
				result += "<Parameter Name='privilegies' Type='String'>-1</Parameter>";
				result += "<Parameter Name='devices' Type='String'></Parameter>";
				result += "<Parameter Name='reqId' Type='String'>" + folder
						+ "</Parameter>";
				result += "<Parameter Name='axoidRef' Type='String'></Parameter>";
				result += "<Parameter Name='op' Type='String'>" + op
						+ "</Parameter>";
				result += "<Parameter Name='wfType' Type='String'>ECLAP</Parameter>"; // TODO change wfType
				result += "<Parameter Name='iprModelId' Type='Integer'></Parameter>";
				result += "</Arguments><Rule_Body/><Dependencies/></AXCP_Rule></Definition>";
				writer.print(result);
				writer.close();
				return result;
			} else {
				System.out.println("File request.req error");
				return null;
			}
		} catch (IOException e) {
			System.err.println(e);
		}
		return null;
	}

	private void error() {
		System.out.println("ERROR! Error updating table");
		System.exit(0);
	}
}