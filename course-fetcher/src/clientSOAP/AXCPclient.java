package clientSOAP;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.xml.rpc.ServiceException;
import org.apache.axis.types.URI.MalformedURIException;

public class AXCPclient {

	private String ruleID;
	private String arguments;
	private String AXRQID;
	private Properties props;

	public AXCPclient(String arguments, String AXRQID) {
		this.arguments = arguments;
		this.AXRQID = AXRQID;
		
		// Load configuration file
		this.props = new Properties();
		try {
			props.load(this.getClass().getResourceAsStream("/config.properties"));
		} catch (FileNotFoundException e) {
			System.err.println(e);
		} catch (IOException e) {
			System.err.println(e);
		}
		
		this.ruleID = props.getProperty("AXCP_BO_UPLOAD_RULE_ID");
	}

	public boolean send() {
		System.out.println("AXCP client starts");
		EngineServiceLocator esl = new EngineServiceLocator();
		EngineSOAP eSOAP = null;
		EngineResult result = null;
		URL url = null;
		org.apache.axis.types.URI endpointSERVER = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		Date date = new Date();
		try {
			url = new URL("http", props.getProperty("AXCP_BO_HOST"), Integer.parseInt(props.getProperty("AXCP_BO_PORT")), "");
			endpointSERVER = new org.apache.axis.types.URI(props.getProperty("AXCP_ENDPOINT_URI"));
			RunRequest request = new RunRequest(dateFormat.format(date), AXRQID, ruleID, "AXCP", endpointSERVER, "admin", arguments);
			System.out.println("Creating request: AXRQID = " + AXRQID + " to: " + url.toString());
			eSOAP = esl.getEngineSOAP(url);
			result = eSOAP.runRule(request);
			//Check result
			if(result.isResult() == true){
				return true;
			}
			return false;
		} catch (ServiceException e) {
			System.err.println(e);
		} catch (RemoteException e) {
			System.err.println(e);
		} catch (MalformedURLException e) {
			System.err.println(e);
		} catch (MalformedURIException e) {
			System.err.println(e);
		}
		return false;
	}

	public String getRuleID() {
		return ruleID;
	}

	public void setRuleID(String ruleID) {
		this.ruleID = ruleID;
	}

	public String getArguments() {
		return arguments;
	}

	public void setArguments(String arguments) {
		this.arguments = arguments;
	}

	public String getAXRQID() {
		return AXRQID;
	}

	public void setAXRQID(String aXRQID) {
		AXRQID = aXRQID;
	}
}
