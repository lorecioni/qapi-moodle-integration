/**
 * KillRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package clientSOAP;

public class KillRequest  implements java.io.Serializable {
    private java.lang.String ruleID;

    private java.lang.String ruleType;

    private java.lang.String AXRQID;

    private java.lang.String userCredentials;

    public KillRequest() {
    }

    public KillRequest(
           java.lang.String ruleID,
           java.lang.String ruleType,
           java.lang.String AXRQID,
           java.lang.String userCredentials) {
           this.ruleID = ruleID;
           this.ruleType = ruleType;
           this.AXRQID = AXRQID;
           this.userCredentials = userCredentials;
    }


    /**
     * Gets the ruleID value for this KillRequest.
     * 
     * @return ruleID
     */
    public java.lang.String getRuleID() {
        return ruleID;
    }


    /**
     * Sets the ruleID value for this KillRequest.
     * 
     * @param ruleID
     */
    public void setRuleID(java.lang.String ruleID) {
        this.ruleID = ruleID;
    }


    /**
     * Gets the ruleType value for this KillRequest.
     * 
     * @return ruleType
     */
    public java.lang.String getRuleType() {
        return ruleType;
    }


    /**
     * Sets the ruleType value for this KillRequest.
     * 
     * @param ruleType
     */
    public void setRuleType(java.lang.String ruleType) {
        this.ruleType = ruleType;
    }


    /**
     * Gets the AXRQID value for this KillRequest.
     * 
     * @return AXRQID
     */
    public java.lang.String getAXRQID() {
        return AXRQID;
    }


    /**
     * Sets the AXRQID value for this KillRequest.
     * 
     * @param AXRQID
     */
    public void setAXRQID(java.lang.String AXRQID) {
        this.AXRQID = AXRQID;
    }


    /**
     * Gets the userCredentials value for this KillRequest.
     * 
     * @return userCredentials
     */
    public java.lang.String getUserCredentials() {
        return userCredentials;
    }


    /**
     * Sets the userCredentials value for this KillRequest.
     * 
     * @param userCredentials
     */
    public void setUserCredentials(java.lang.String userCredentials) {
        this.userCredentials = userCredentials;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof KillRequest)) return false;
        KillRequest other = (KillRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ruleID==null && other.getRuleID()==null) || 
             (this.ruleID!=null &&
              this.ruleID.equals(other.getRuleID()))) &&
            ((this.ruleType==null && other.getRuleType()==null) || 
             (this.ruleType!=null &&
              this.ruleType.equals(other.getRuleType()))) &&
            ((this.AXRQID==null && other.getAXRQID()==null) || 
             (this.AXRQID!=null &&
              this.AXRQID.equals(other.getAXRQID()))) &&
            ((this.userCredentials==null && other.getUserCredentials()==null) || 
             (this.userCredentials!=null &&
              this.userCredentials.equals(other.getUserCredentials())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRuleID() != null) {
            _hashCode += getRuleID().hashCode();
        }
        if (getRuleType() != null) {
            _hashCode += getRuleType().hashCode();
        }
        if (getAXRQID() != null) {
            _hashCode += getAXRQID().hashCode();
        }
        if (getUserCredentials() != null) {
            _hashCode += getUserCredentials().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(KillRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">killRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RuleID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RuleType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AXRQID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AXRQID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userCredentials");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UserCredentials"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
