/**
 * EngineResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package clientSOAP;

public class EngineResult  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean result;

    private java.lang.String errorMSG;

    private java.lang.String errorCode;

    private java.lang.String status;

    private java.lang.String ruleIDs;

    private java.lang.String ruleLogs;

    private java.lang.String ruleSchema;

    private java.lang.String programIDs;

    public EngineResult() {
    }

    public EngineResult(
           boolean result,
           java.lang.String errorMSG,
           java.lang.String errorCode,
           java.lang.String status,
           java.lang.String ruleIDs,
           java.lang.String ruleLogs,
           java.lang.String ruleSchema,
           java.lang.String programIDs) {
           this.result = result;
           this.errorMSG = errorMSG;
           this.errorCode = errorCode;
           this.status = status;
           this.ruleIDs = ruleIDs;
           this.ruleLogs = ruleLogs;
           this.ruleSchema = ruleSchema;
           this.programIDs = programIDs;
    }


    /**
     * Gets the result value for this EngineResult.
     * 
     * @return result
     */
    public boolean isResult() {
        return result;
    }


    /**
     * Sets the result value for this EngineResult.
     * 
     * @param result
     */
    public void setResult(boolean result) {
        this.result = result;
    }


    /**
     * Gets the errorMSG value for this EngineResult.
     * 
     * @return errorMSG
     */
    public java.lang.String getErrorMSG() {
        return errorMSG;
    }


    /**
     * Sets the errorMSG value for this EngineResult.
     * 
     * @param errorMSG
     */
    public void setErrorMSG(java.lang.String errorMSG) {
        this.errorMSG = errorMSG;
    }


    /**
     * Gets the errorCode value for this EngineResult.
     * 
     * @return errorCode
     */
    public java.lang.String getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this EngineResult.
     * 
     * @param errorCode
     */
    public void setErrorCode(java.lang.String errorCode) {
        this.errorCode = errorCode;
    }


    /**
     * Gets the status value for this EngineResult.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this EngineResult.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the ruleIDs value for this EngineResult.
     * 
     * @return ruleIDs
     */
    public java.lang.String getRuleIDs() {
        return ruleIDs;
    }


    /**
     * Sets the ruleIDs value for this EngineResult.
     * 
     * @param ruleIDs
     */
    public void setRuleIDs(java.lang.String ruleIDs) {
        this.ruleIDs = ruleIDs;
    }


    /**
     * Gets the ruleLogs value for this EngineResult.
     * 
     * @return ruleLogs
     */
    public java.lang.String getRuleLogs() {
        return ruleLogs;
    }


    /**
     * Sets the ruleLogs value for this EngineResult.
     * 
     * @param ruleLogs
     */
    public void setRuleLogs(java.lang.String ruleLogs) {
        this.ruleLogs = ruleLogs;
    }


    /**
     * Gets the ruleSchema value for this EngineResult.
     * 
     * @return ruleSchema
     */
    public java.lang.String getRuleSchema() {
        return ruleSchema;
    }


    /**
     * Sets the ruleSchema value for this EngineResult.
     * 
     * @param ruleSchema
     */
    public void setRuleSchema(java.lang.String ruleSchema) {
        this.ruleSchema = ruleSchema;
    }


    /**
     * Gets the programIDs value for this EngineResult.
     * 
     * @return programIDs
     */
    public java.lang.String getProgramIDs() {
        return programIDs;
    }


    /**
     * Sets the programIDs value for this EngineResult.
     * 
     * @param programIDs
     */
    public void setProgramIDs(java.lang.String programIDs) {
        this.programIDs = programIDs;
    }

    private java.lang.Object __equalsCalc = null;
    @SuppressWarnings("unused")
	public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EngineResult)) return false;
        EngineResult other = (EngineResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.result == other.isResult() &&
            ((this.errorMSG==null && other.getErrorMSG()==null) || 
             (this.errorMSG!=null &&
              this.errorMSG.equals(other.getErrorMSG()))) &&
            ((this.errorCode==null && other.getErrorCode()==null) || 
             (this.errorCode!=null &&
              this.errorCode.equals(other.getErrorCode()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.ruleIDs==null && other.getRuleIDs()==null) || 
             (this.ruleIDs!=null &&
              this.ruleIDs.equals(other.getRuleIDs()))) &&
            ((this.ruleLogs==null && other.getRuleLogs()==null) || 
             (this.ruleLogs!=null &&
              this.ruleLogs.equals(other.getRuleLogs()))) &&
            ((this.ruleSchema==null && other.getRuleSchema()==null) || 
             (this.ruleSchema!=null &&
              this.ruleSchema.equals(other.getRuleSchema()))) &&
            ((this.programIDs==null && other.getProgramIDs()==null) || 
             (this.programIDs!=null &&
              this.programIDs.equals(other.getProgramIDs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += (isResult() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getErrorMSG() != null) {
            _hashCode += getErrorMSG().hashCode();
        }
        if (getErrorCode() != null) {
            _hashCode += getErrorCode().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getRuleIDs() != null) {
            _hashCode += getRuleIDs().hashCode();
        }
        if (getRuleLogs() != null) {
            _hashCode += getRuleLogs().hashCode();
        }
        if (getRuleSchema() != null) {
            _hashCode += getRuleSchema().hashCode();
        }
        if (getProgramIDs() != null) {
            _hashCode += getProgramIDs().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EngineResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMSG");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ErrorMSG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ErrorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleIDs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ruleIDs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleLogs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ruleLogs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleSchema");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ruleSchema"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("programIDs");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ProgramIDs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
