/**
 * EngineService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package clientSOAP;

public interface EngineService extends javax.xml.rpc.Service {
    public java.lang.String getEngineSOAPAddress();

    public clientSOAP.EngineSOAP getEngineSOAP() throws javax.xml.rpc.ServiceException;

    public clientSOAP.EngineSOAP getEngineSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
