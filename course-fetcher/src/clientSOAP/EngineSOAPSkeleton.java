/**
 * EngineSOAPSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package clientSOAP;

import java.rmi.RemoteException;

public class EngineSOAPSkeleton implements clientSOAP.EngineSOAP, org.apache.axis.wsdl.Skeleton {
    private clientSOAP.EngineSOAP impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "installRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">installRequest"), clientSOAP.InstallRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("installAndActivate", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "InstallAndActivate"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/InstallAndActivateRule");
        _myOperationsList.add(_oper);
        if (_myOperations.get("installAndActivate") == null) {
            _myOperations.put("installAndActivate", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("installAndActivate")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "runRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">runRequest"), clientSOAP.RunRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("runRule", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "RunRule"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/RunRule");
        _myOperationsList.add(_oper);
        if (_myOperations.get("runRule") == null) {
            _myOperations.put("runRule", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("runRule")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "deactivateRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">deactivateRequest"), clientSOAP.DeactivateRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("deactivateRule", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "DeactivateRule"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/DeactivateRule");
        _myOperationsList.add(_oper);
        if (_myOperations.get("deactivateRule") == null) {
            _myOperations.put("deactivateRule", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("deactivateRule")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "suspendRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">suspendRequest"), clientSOAP.SuspendRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("suspendRule", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "SuspendRule"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/SuspendRule");
        _myOperationsList.add(_oper);
        if (_myOperations.get("suspendRule") == null) {
            _myOperations.put("suspendRule", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("suspendRule")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "pauseRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pauseRequest"), clientSOAP.PauseRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("pauseRule", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "PauseRule"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/PauseRule");
        _myOperationsList.add(_oper);
        if (_myOperations.get("pauseRule") == null) {
            _myOperations.put("pauseRule", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("pauseRule")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "killRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">killRequest"), clientSOAP.KillRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("killRule", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "KillRule"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/KillRule");
        _myOperationsList.add(_oper);
        if (_myOperations.get("killRule") == null) {
            _myOperations.put("killRule", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("killRule")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "removeRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">removeRequest"), clientSOAP.RemoveRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("removeRule", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "RemoveRule"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/RemoveRule");
        _myOperationsList.add(_oper);
        if (_myOperations.get("removeRule") == null) {
            _myOperations.put("removeRule", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("removeRule")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "resumeRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">resumeRequest"), clientSOAP.ResumeRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("resumeRule", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "ResumeRule"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/ResumeRule");
        _myOperationsList.add(_oper);
        if (_myOperations.get("resumeRule") == null) {
            _myOperations.put("resumeRule", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("resumeRule")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "getStatusRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">getStatusRequest"), clientSOAP.GetStatusRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getRuleStatus", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetRuleStatus"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/GetRuleStatus");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getRuleStatus") == null) {
            _myOperations.put("getRuleStatus", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getRuleStatus")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "getLogRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">getLogRequest"), clientSOAP.GetLogRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getRuleLog", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetRuleLog"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/GetRuleLog");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getRuleLog") == null) {
            _myOperations.put("getRuleLog", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getRuleLog")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "getListRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">getListRequest"), clientSOAP.GetListRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getListofRules", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetListofRules"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/GetListofRules");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getListofRules") == null) {
            _myOperations.put("getListofRules", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getListofRules")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "getRuleRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">getRuleRequest"), clientSOAP.GetRuleRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getRule", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetRule"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/GetRule");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getRule") == null) {
            _myOperations.put("getRule", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getRule")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "pnpStatusRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pnpStatusRequest"), clientSOAP.PnpStatusRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("statusRequestPnP", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "StatusRequestPnP"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/StatusRequestPnP");
        _myOperationsList.add(_oper);
        if (_myOperations.get("statusRequestPnP") == null) {
            _myOperations.put("statusRequestPnP", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("statusRequestPnP")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "pnpAbortRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pnpAbortRequest"), clientSOAP.PnpAbortRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("abortPnPProgram", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "AbortPnPProgram"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/AbortPnPProgram");
        _myOperationsList.add(_oper);
        if (_myOperations.get("abortPnPProgram") == null) {
            _myOperations.put("abortPnPProgram", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("abortPnPProgram")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "pnpActivateRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pnpActivateRequest"), clientSOAP.PnpActivateRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("activatePnPProgram", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "ActivatePnPProgram"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/ActivatePnPProgram");
        _myOperationsList.add(_oper);
        if (_myOperations.get("activatePnPProgram") == null) {
            _myOperations.put("activatePnPProgram", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("activatePnPProgram")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "wfNotificationRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">wfNotificationRequest"), clientSOAP.WfNotificationRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("WFNotification", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "WFNotification"));
        _oper.setSoapAction("http://www.axmedis.org/WF/Engine/WFNotification");
        _myOperationsList.add(_oper);
        if (_myOperations.get("WFNotification") == null) {
            _myOperations.put("WFNotification", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("WFNotification")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "pnpGetListRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pnpGetListRequest"), clientSOAP.PnpGetListRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getListofPrograms", _params, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("", "GetListofPrograms"));
        _oper.setSoapAction("urn:#GetListofPrograms");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getListofPrograms") == null) {
            _myOperations.put("getListofPrograms", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getListofPrograms")).add(_oper);
    }

    public EngineSOAPSkeleton() {
        this.impl = new clientSOAP.EngineSOAPImpl();
    }

    public EngineSOAPSkeleton(clientSOAP.EngineSOAP impl) {
        this.impl = impl;
    }
    public clientSOAP.EngineResult installAndActivate(clientSOAP.InstallRequest installRequest) throws java.rmi.RemoteException
    {
        clientSOAP.EngineResult ret = impl.installAndActivate(installRequest);
        return ret;
    }

    public clientSOAP.EngineResult runRule(clientSOAP.RunRequest runRequest) throws java.rmi.RemoteException
    {
        clientSOAP.EngineResult ret = impl.runRule(runRequest);
        return ret;
    }

    public clientSOAP.EngineResult suspendRule(clientSOAP.SuspendRequest suspendRequest) throws java.rmi.RemoteException
    {
        clientSOAP.EngineResult ret = impl.suspendRule(suspendRequest);
        return ret;
    }

    public clientSOAP.EngineResult pauseRule(clientSOAP.PauseRequest pauseRequest) throws java.rmi.RemoteException
    {
        clientSOAP.EngineResult ret = impl.pauseRule(pauseRequest);
        return ret;
    }


    public clientSOAP.EngineResult resumeRule(clientSOAP.ResumeRequest resumeRequest) throws java.rmi.RemoteException
    {
        clientSOAP.EngineResult ret = impl.resumeRule(resumeRequest);
        return ret;
    }

    public clientSOAP.EngineResult getListofRules(clientSOAP.GetListRequest getListRequest) throws java.rmi.RemoteException
    {
        clientSOAP.EngineResult ret = impl.getListofRules(getListRequest);
        return ret;
    }

	@Override
	public EngineResult deactivateRule(DeactivateRequest deactivateRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EngineResult killRule(KillRequest killRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EngineResult removeRule(RemoveRequest removeRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EngineResult getRuleStatus(GetStatusRequest getStatusRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EngineResult getRuleLog(GetLogRequest getLogRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EngineResult getRule(GetRuleRequest getRuleRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EngineResult statusRequestPnP(PnpStatusRequest pnpStatusRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EngineResult abortPnPProgram(PnpAbortRequest pnpAbortRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EngineResult activatePnPProgram(PnpActivateRequest pnpActivateRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EngineResult WFNotification(
			WfNotificationRequest wfNotificationRequest) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EngineResult getListofPrograms(PnpGetListRequest pnpGetListRequest)
			throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}
}
