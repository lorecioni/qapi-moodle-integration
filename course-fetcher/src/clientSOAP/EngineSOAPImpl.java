/**
 * EngineSOAPImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package clientSOAP;

public class EngineSOAPImpl implements clientSOAP.EngineSOAP{
    public clientSOAP.EngineResult installAndActivate(clientSOAP.InstallRequest installRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult runRule(clientSOAP.RunRequest runRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult deactivateRule(clientSOAP.DeactivateRequest deactivateRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult suspendRule(clientSOAP.SuspendRequest suspendRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult pauseRule(clientSOAP.PauseRequest pauseRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult killRule(clientSOAP.KillRequest killRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult removeRule(clientSOAP.RemoveRequest removeRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult resumeRule(clientSOAP.ResumeRequest resumeRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult getRuleStatus(clientSOAP.GetStatusRequest getStatusRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult getRuleLog(clientSOAP.GetLogRequest getLogRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult getListofRules(clientSOAP.GetListRequest getListRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult getRule(clientSOAP.GetRuleRequest getRuleRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult statusRequestPnP(clientSOAP.PnpStatusRequest pnpStatusRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult abortPnPProgram(clientSOAP.PnpAbortRequest pnpAbortRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult activatePnPProgram(clientSOAP.PnpActivateRequest pnpActivateRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult WFNotification(clientSOAP.WfNotificationRequest wfNotificationRequest) throws java.rmi.RemoteException {
        return null;
    }

    public clientSOAP.EngineResult getListofPrograms(clientSOAP.PnpGetListRequest pnpGetListRequest) throws java.rmi.RemoteException {
        return null;
    }

}
