/**
 * PnpActivateRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package clientSOAP;

public class PnpActivateRequest  implements java.io.Serializable {
    private java.lang.String programme;

    private java.lang.String AXRQID;

    private java.lang.String userCredentials;

    private org.apache.axis.types.URI engineListenerService;

    public PnpActivateRequest() {
    }

    public PnpActivateRequest(
           java.lang.String programme,
           java.lang.String AXRQID,
           java.lang.String userCredentials,
           org.apache.axis.types.URI engineListenerService) {
           this.programme = programme;
           this.AXRQID = AXRQID;
           this.userCredentials = userCredentials;
           this.engineListenerService = engineListenerService;
    }


    /**
     * Gets the programme value for this PnpActivateRequest.
     * 
     * @return programme
     */
    public java.lang.String getProgramme() {
        return programme;
    }


    /**
     * Sets the programme value for this PnpActivateRequest.
     * 
     * @param programme
     */
    public void setProgramme(java.lang.String programme) {
        this.programme = programme;
    }


    /**
     * Gets the AXRQID value for this PnpActivateRequest.
     * 
     * @return AXRQID
     */
    public java.lang.String getAXRQID() {
        return AXRQID;
    }


    /**
     * Sets the AXRQID value for this PnpActivateRequest.
     * 
     * @param AXRQID
     */
    public void setAXRQID(java.lang.String AXRQID) {
        this.AXRQID = AXRQID;
    }


    /**
     * Gets the userCredentials value for this PnpActivateRequest.
     * 
     * @return userCredentials
     */
    public java.lang.String getUserCredentials() {
        return userCredentials;
    }


    /**
     * Sets the userCredentials value for this PnpActivateRequest.
     * 
     * @param userCredentials
     */
    public void setUserCredentials(java.lang.String userCredentials) {
        this.userCredentials = userCredentials;
    }


    /**
     * Gets the engineListenerService value for this PnpActivateRequest.
     * 
     * @return engineListenerService
     */
    public org.apache.axis.types.URI getEngineListenerService() {
        return engineListenerService;
    }


    /**
     * Sets the engineListenerService value for this PnpActivateRequest.
     * 
     * @param engineListenerService
     */
    public void setEngineListenerService(org.apache.axis.types.URI engineListenerService) {
        this.engineListenerService = engineListenerService;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PnpActivateRequest)) return false;
        PnpActivateRequest other = (PnpActivateRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.programme==null && other.getProgramme()==null) || 
             (this.programme!=null &&
              this.programme.equals(other.getProgramme()))) &&
            ((this.AXRQID==null && other.getAXRQID()==null) || 
             (this.AXRQID!=null &&
              this.AXRQID.equals(other.getAXRQID()))) &&
            ((this.userCredentials==null && other.getUserCredentials()==null) || 
             (this.userCredentials!=null &&
              this.userCredentials.equals(other.getUserCredentials()))) &&
            ((this.engineListenerService==null && other.getEngineListenerService()==null) || 
             (this.engineListenerService!=null &&
              this.engineListenerService.equals(other.getEngineListenerService())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProgramme() != null) {
            _hashCode += getProgramme().hashCode();
        }
        if (getAXRQID() != null) {
            _hashCode += getAXRQID().hashCode();
        }
        if (getUserCredentials() != null) {
            _hashCode += getUserCredentials().hashCode();
        }
        if (getEngineListenerService() != null) {
            _hashCode += getEngineListenerService().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PnpActivateRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pnpActivateRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("programme");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Programme"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AXRQID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AXRQID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userCredentials");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UserCredentials"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("engineListenerService");
        elemField.setXmlName(new javax.xml.namespace.QName("", "EngineListenerService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyURI"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
