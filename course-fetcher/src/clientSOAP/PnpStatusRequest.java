/**
 * PnpStatusRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package clientSOAP;

public class PnpStatusRequest  implements java.io.Serializable {
    private java.lang.String programID;

    private java.lang.String AXRQID;

    private java.lang.String userCredentials;

    public PnpStatusRequest() {
    }

    public PnpStatusRequest(
           java.lang.String programID,
           java.lang.String AXRQID,
           java.lang.String userCredentials) {
           this.programID = programID;
           this.AXRQID = AXRQID;
           this.userCredentials = userCredentials;
    }


    /**
     * Gets the programID value for this PnpStatusRequest.
     * 
     * @return programID
     */
    public java.lang.String getProgramID() {
        return programID;
    }


    /**
     * Sets the programID value for this PnpStatusRequest.
     * 
     * @param programID
     */
    public void setProgramID(java.lang.String programID) {
        this.programID = programID;
    }


    /**
     * Gets the AXRQID value for this PnpStatusRequest.
     * 
     * @return AXRQID
     */
    public java.lang.String getAXRQID() {
        return AXRQID;
    }


    /**
     * Sets the AXRQID value for this PnpStatusRequest.
     * 
     * @param AXRQID
     */
    public void setAXRQID(java.lang.String AXRQID) {
        this.AXRQID = AXRQID;
    }


    /**
     * Gets the userCredentials value for this PnpStatusRequest.
     * 
     * @return userCredentials
     */
    public java.lang.String getUserCredentials() {
        return userCredentials;
    }


    /**
     * Sets the userCredentials value for this PnpStatusRequest.
     * 
     * @param userCredentials
     */
    public void setUserCredentials(java.lang.String userCredentials) {
        this.userCredentials = userCredentials;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PnpStatusRequest)) return false;
        PnpStatusRequest other = (PnpStatusRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.programID==null && other.getProgramID()==null) || 
             (this.programID!=null &&
              this.programID.equals(other.getProgramID()))) &&
            ((this.AXRQID==null && other.getAXRQID()==null) || 
             (this.AXRQID!=null &&
              this.AXRQID.equals(other.getAXRQID()))) &&
            ((this.userCredentials==null && other.getUserCredentials()==null) || 
             (this.userCredentials!=null &&
              this.userCredentials.equals(other.getUserCredentials())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProgramID() != null) {
            _hashCode += getProgramID().hashCode();
        }
        if (getAXRQID() != null) {
            _hashCode += getAXRQID().hashCode();
        }
        if (getUserCredentials() != null) {
            _hashCode += getUserCredentials().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PnpStatusRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pnpStatusRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("programID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ProgramID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AXRQID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AXRQID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userCredentials");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UserCredentials"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
