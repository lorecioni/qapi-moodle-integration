/**
 * EngineSOAPStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package clientSOAP;

public class EngineSOAPStub extends org.apache.axis.client.Stub implements clientSOAP.EngineSOAP {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[17];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("InstallAndActivate");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "installRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">installRequest"), clientSOAP.InstallRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RunRule");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "runRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">runRequest"), clientSOAP.RunRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("DeactivateRule");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "deactivateRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">deactivateRequest"), clientSOAP.DeactivateRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SuspendRule");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "suspendRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">suspendRequest"), clientSOAP.SuspendRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("PauseRule");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "pauseRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pauseRequest"), clientSOAP.PauseRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("KillRule");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "killRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">killRequest"), clientSOAP.KillRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RemoveRule");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "removeRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">removeRequest"), clientSOAP.RemoveRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ResumeRule");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "resumeRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">resumeRequest"), clientSOAP.ResumeRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetRuleStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "getStatusRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">getStatusRequest"), clientSOAP.GetStatusRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetRuleLog");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "getLogRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">getLogRequest"), clientSOAP.GetLogRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetListofRules");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "getListRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">getListRequest"), clientSOAP.GetListRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetRule");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "getRuleRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">getRuleRequest"), clientSOAP.GetRuleRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("StatusRequestPnP");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "pnpStatusRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pnpStatusRequest"), clientSOAP.PnpStatusRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("AbortPnPProgram");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "pnpAbortRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pnpAbortRequest"), clientSOAP.PnpAbortRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ActivatePnPProgram");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "pnpActivateRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pnpActivateRequest"), clientSOAP.PnpActivateRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("WFNotification");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "wfNotificationRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">wfNotificationRequest"), clientSOAP.WfNotificationRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetListofPrograms");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "pnpGetListRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pnpGetListRequest"), clientSOAP.PnpGetListRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult"));
        oper.setReturnClass(clientSOAP.EngineResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", "EngineResult"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

    }

    public EngineSOAPStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public EngineSOAPStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public EngineSOAPStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">deactivateRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.DeactivateRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">EngineResult");
            cachedSerQNames.add(qName);
            cls = clientSOAP.EngineResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">getListRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.GetListRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">getLogRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.GetLogRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">getRuleRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.GetRuleRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">getStatusRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.GetStatusRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">installRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.InstallRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">killRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.KillRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pauseRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.PauseRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pnpAbortRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.PnpAbortRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pnpActivateRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.PnpActivateRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pnpGetListRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.PnpGetListRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">pnpStatusRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.PnpStatusRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">removeRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.RemoveRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">resumeRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.ResumeRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">runRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.RunRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">suspendRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.SuspendRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">wfNotificationRequest");
            cachedSerQNames.add(qName);
            cls = clientSOAP.WfNotificationRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public clientSOAP.EngineResult installAndActivate(clientSOAP.InstallRequest installRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/InstallAndActivateRule");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "InstallAndActivate"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {installRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult runRule(clientSOAP.RunRequest runRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/RunRule");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "RunRule"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {runRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult deactivateRule(clientSOAP.DeactivateRequest deactivateRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/DeactivateRule");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "DeactivateRule"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {deactivateRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult suspendRule(clientSOAP.SuspendRequest suspendRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/SuspendRule");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "SuspendRule"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {suspendRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult pauseRule(clientSOAP.PauseRequest pauseRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/PauseRule");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "PauseRule"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {pauseRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult killRule(clientSOAP.KillRequest killRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/KillRule");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "KillRule"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {killRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult removeRule(clientSOAP.RemoveRequest removeRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/RemoveRule");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "RemoveRule"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {removeRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult resumeRule(clientSOAP.ResumeRequest resumeRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/ResumeRule");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ResumeRule"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {resumeRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult getRuleStatus(clientSOAP.GetStatusRequest getStatusRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/GetRuleStatus");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetRuleStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getStatusRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult getRuleLog(clientSOAP.GetLogRequest getLogRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/GetRuleLog");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetRuleLog"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getLogRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult getListofRules(clientSOAP.GetListRequest getListRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/GetListofRules");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetListofRules"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getListRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult getRule(clientSOAP.GetRuleRequest getRuleRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/GetRule");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetRule"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getRuleRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult statusRequestPnP(clientSOAP.PnpStatusRequest pnpStatusRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/StatusRequestPnP");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "StatusRequestPnP"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {pnpStatusRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult abortPnPProgram(clientSOAP.PnpAbortRequest pnpAbortRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/AbortPnPProgram");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "AbortPnPProgram"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {pnpAbortRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult activatePnPProgram(clientSOAP.PnpActivateRequest pnpActivateRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/ActivatePnPProgram");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "ActivatePnPProgram"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {pnpActivateRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult WFNotification(clientSOAP.WfNotificationRequest wfNotificationRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.axmedis.org/WF/Engine/WFNotification");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "WFNotification"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {wfNotificationRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public clientSOAP.EngineResult getListofPrograms(clientSOAP.PnpGetListRequest pnpGetListRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:#GetListofPrograms");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "GetListofPrograms"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {pnpGetListRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (clientSOAP.EngineResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (clientSOAP.EngineResult) org.apache.axis.utils.JavaUtils.convert(_resp, clientSOAP.EngineResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
