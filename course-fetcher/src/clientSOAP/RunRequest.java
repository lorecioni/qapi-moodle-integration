/**
 * RunRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package clientSOAP;

public class RunRequest  implements java.io.Serializable {
    private java.lang.String time;

    private java.lang.String AXRQID;

    private java.lang.String ruleID;

    private java.lang.String ruleType;

    private org.apache.axis.types.URI engineListenerService;

    private java.lang.String userCredentials;

    private java.lang.String arguments;

    public RunRequest() {
    }

    public RunRequest(
           java.lang.String time,
           java.lang.String AXRQID,
           java.lang.String ruleID,
           java.lang.String ruleType,
           org.apache.axis.types.URI engineListenerService,
           java.lang.String userCredentials,
           java.lang.String arguments) {
           this.time = time;
           this.AXRQID = AXRQID;
           this.ruleID = ruleID;
           this.ruleType = ruleType;
           this.engineListenerService = engineListenerService;
           this.userCredentials = userCredentials;
           this.arguments = arguments;
    }


    /**
     * Gets the time value for this RunRequest.
     * 
     * @return time
     */
    public java.lang.String getTime() {
        return time;
    }


    /**
     * Sets the time value for this RunRequest.
     * 
     * @param time
     */
    public void setTime(java.lang.String time) {
        this.time = time;
    }


    /**
     * Gets the AXRQID value for this RunRequest.
     * 
     * @return AXRQID
     */
    public java.lang.String getAXRQID() {
        return AXRQID;
    }


    /**
     * Sets the AXRQID value for this RunRequest.
     * 
     * @param AXRQID
     */
    public void setAXRQID(java.lang.String AXRQID) {
        this.AXRQID = AXRQID;
    }


    /**
     * Gets the ruleID value for this RunRequest.
     * 
     * @return ruleID
     */
    public java.lang.String getRuleID() {
        return ruleID;
    }


    /**
     * Sets the ruleID value for this RunRequest.
     * 
     * @param ruleID
     */
    public void setRuleID(java.lang.String ruleID) {
        this.ruleID = ruleID;
    }


    /**
     * Gets the ruleType value for this RunRequest.
     * 
     * @return ruleType
     */
    public java.lang.String getRuleType() {
        return ruleType;
    }


    /**
     * Sets the ruleType value for this RunRequest.
     * 
     * @param ruleType
     */
    public void setRuleType(java.lang.String ruleType) {
        this.ruleType = ruleType;
    }


    /**
     * Gets the engineListenerService value for this RunRequest.
     * 
     * @return engineListenerService
     */
    public org.apache.axis.types.URI getEngineListenerService() {
        return engineListenerService;
    }


    /**
     * Sets the engineListenerService value for this RunRequest.
     * 
     * @param engineListenerService
     */
    public void setEngineListenerService(org.apache.axis.types.URI engineListenerService) {
        this.engineListenerService = engineListenerService;
    }


    /**
     * Gets the userCredentials value for this RunRequest.
     * 
     * @return userCredentials
     */
    public java.lang.String getUserCredentials() {
        return userCredentials;
    }


    /**
     * Sets the userCredentials value for this RunRequest.
     * 
     * @param userCredentials
     */
    public void setUserCredentials(java.lang.String userCredentials) {
        this.userCredentials = userCredentials;
    }


    /**
     * Gets the arguments value for this RunRequest.
     * 
     * @return arguments
     */
    public java.lang.String getArguments() {
        return arguments;
    }


    /**
     * Sets the arguments value for this RunRequest.
     * 
     * @param arguments
     */
    public void setArguments(java.lang.String arguments) {
        this.arguments = arguments;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RunRequest)) return false;
        RunRequest other = (RunRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.time==null && other.getTime()==null) || 
             (this.time!=null &&
              this.time.equals(other.getTime()))) &&
            ((this.AXRQID==null && other.getAXRQID()==null) || 
             (this.AXRQID!=null &&
              this.AXRQID.equals(other.getAXRQID()))) &&
            ((this.ruleID==null && other.getRuleID()==null) || 
             (this.ruleID!=null &&
              this.ruleID.equals(other.getRuleID()))) &&
            ((this.ruleType==null && other.getRuleType()==null) || 
             (this.ruleType!=null &&
              this.ruleType.equals(other.getRuleType()))) &&
            ((this.engineListenerService==null && other.getEngineListenerService()==null) || 
             (this.engineListenerService!=null &&
              this.engineListenerService.equals(other.getEngineListenerService()))) &&
            ((this.userCredentials==null && other.getUserCredentials()==null) || 
             (this.userCredentials!=null &&
              this.userCredentials.equals(other.getUserCredentials()))) &&
            ((this.arguments==null && other.getArguments()==null) || 
             (this.arguments!=null &&
              this.arguments.equals(other.getArguments())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTime() != null) {
            _hashCode += getTime().hashCode();
        }
        if (getAXRQID() != null) {
            _hashCode += getAXRQID().hashCode();
        }
        if (getRuleID() != null) {
            _hashCode += getRuleID().hashCode();
        }
        if (getRuleType() != null) {
            _hashCode += getRuleType().hashCode();
        }
        if (getEngineListenerService() != null) {
            _hashCode += getEngineListenerService().hashCode();
        }
        if (getUserCredentials() != null) {
            _hashCode += getUserCredentials().hashCode();
        }
        if (getArguments() != null) {
            _hashCode += getArguments().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RunRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/", ">runRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("time");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Time"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AXRQID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AXRQID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RuleID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RuleType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("engineListenerService");
        elemField.setXmlName(new javax.xml.namespace.QName("", "EngineListenerService"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyURI"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userCredentials");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UserCredentials"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("arguments");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Arguments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
