<!DOCTYPE html>
<html>
<?php
if (!file_exists('../config.php')) {
    header('HTTP/1.1 403 Forbidden');
    header('Status: 403 Forbidden');
    echo 'Moodle not properly installed!';
}

require_once('../config.php');

$sql = "SELECT * FROM last_update";
		
$courses = get_records_sql($sql);
?>
<script type="text/javascript">
	function SetAllCheckBoxes(FormName, CheckValue) {
		if(!document.forms[FormName]) {return;}
		var objCheckBoxes = document.forms[FormName].elements;
		if(!objCheckBoxes){ return;}
		var countCheckBoxes = objCheckBoxes.length;
		if(!countCheckBoxes){
			objCheckBoxes.checked = CheckValue;}
		else{
		// set the check value for all check boxes
			for(var i = 0; i < countCheckBoxes; i++)
			objCheckBoxes[i].checked = CheckValue;}
		}
</script>
<head>
<title>Eclap Course Manager</title>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div id="wrapper">
		<h1>Eclap Course Manager</h1>
		<form id="select-form" name="select-courses" method="post" action="course_manager.php">
		<div id="course-list">
			<div id="header-list">
				<div class="select row"></div>
				<div class="id row">Id</div>
				<div class="name row">Name</div>
				<div class="mod row">Last Modified</div>
				<div class="proc row">Processed</div>
				<div class="index row">Indexed</div>
				<div class="axoid row">AXOID</div>
				<div class="message row">Message</div>
				<div class="header-status status row">Status</div>
			</div>
			<?php
			 	foreach($courses as $course) {
					$last_modified = $course->last_modified;
					$processed = $course->processed;
					$indexed = $course->indexed;
					$axoid = $course->AXOID;
					$message = $course->message;
					$status = $course->status;		
					$course = get_record("course", "id", $course->course_id);
					echo '<div class="course-item">';
					echo '<input type="checkbox" name="courses[]" value="'.$course->id.'">';
					echo '<div class="id row">'.$course->id.'</div>';
					echo '<div class="name row">'.$course->fullname.'</div>';
					echo '<div class="mod row">'.$last_modified.'</div>';
					echo '<div class="proc row">'.$processed.'</div>';

					$processed = strtotime($processed);
					$date = date_create();
					$now = date_timestamp_get($date);

					$src = '';

					if($status == 'error') {
						$src = 'img/error.png';
					} elseif($status == 'pending') {
						$src = 'img/pending.png';
					} elseif($status == 'indexed'){
						$src = 'img/check.png';
					}

					echo '<div class="index row">'.$indexed.'</div>';
					echo '<div class="axoid row">'.$axoid.'</div>';
					echo '<div class="message row">'.$message.'</div>';
					echo '<div class="status row">';
					echo '<img alt="icon" src="'.$src.'"></div>';
					echo '</div>';				
				}
			?>
			<div class="launch-button row">
				<input name="select-all" type="button" value="Select All" class="button" onclick="SetAllCheckBoxes('select-courses', true);">
				<input name="launch" type="submit" id="cmd-button" class="button" value="Launch selected">
				<input name="launch-all" type="submit" class="button" value="Launch all">
			</div>
		</div>
	</div>
	<?php if(isset($_POST['launch'])){
		$courses_id = $_POST['courses'];
		
		$opts = '';

		foreach ($courses_id as $id){
			$opts .= " ".$id;
		}
		
		if($opts != '') {
			$output = shell_exec('java -jar course-fetcher.jar'.$opts);
			echo "<div id='shell-output'><h2>Output</h2><pre>".$output."</pre></div>";
		} else {
			echo "<div id='shell-output'><h2>No course selected!</h2></div>";
		}
	}

	if(isset($_POST['launch-all'])){
			$output = shell_exec('java -jar course-fetcher.jar');
			echo "<div id='shell-output'><h2>Output</h2><pre>".$output."</pre></div>";
	}?>
</body>
</html>
