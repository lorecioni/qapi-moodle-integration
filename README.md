Moodle course fetcher
=====================

Moodle course indexing under Drupal. Generation of xml structure for courses and indexing in database. Distribuited System project, 2013.

qAPI
----

Install first qAPI plugin in Moodle. Read *qapi/INSTALL.txt* for instructions.

- Request Moodle courses' id

`http://[HOST]/moodle/qapi/rest.php/courseid/`

- Request for the XML version of a Moodle course

`http://[HOST]/moodle/qapi/rest.php/coursexml/[COURSE_ID]`

Database
--------

Execute the *database.sql* script in your database.

Eclap soap server
-----------------

Open the WAR file and edit */WEB-INF/classes/config.properties* with your custom values.
Deploy the WAR file under Tomcat manager. The server is ready. 


Course Fetcher
--------------

Copy the web directory in your web server. Make *course-fetcher.jar* executable.
Navigate for *course_manager.php* and start indexing!
