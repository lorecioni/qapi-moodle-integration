/**
 * EngineChannelClassSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package org.axmedis.www.wf.engine;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Properties;

/**
 * EngineChannelClassSkeleton java skeleton for the axisService
 */
public class EngineChannelClassSkeleton {

	/**
	 * Auto generated method signature
	 * 
	 * @param wFProcess
	 * @return wFProcessResponse
	 */

	public org.axmedis.www.wf.engine.WFProcessResponse wFProcess(
			org.axmedis.www.wf.engine.WFProcess wFProcess) {
		// TODO : fill this with the necessary business logic
		throw new java.lang.UnsupportedOperationException("Please implement "
				+ this.getClass().getName() + "#wFProcess");
	}

	/**
	 * Auto generated method signature
	 * 
	 * @param engineNotification
	 * @return engineNotificationResponse engineNotificationResponse
	 */

	public org.axmedis.www.wf.engine.EngineNotificationResponse engineNotification(
			org.axmedis.www.wf.engine.EngineNotification engineNotification) {

		String log = "-------------------------------";

//		// Carica il file di configurazione
		Properties props = new Properties();
		try {
			props.load(this.getClass().getResourceAsStream("/config.properties"));
		} catch (FileNotFoundException e) {
			log += e;
		} catch (IOException e) {
			log += e;
		}	

		String query = "";
		Connection cc = null;
	
		Date date = new Date();
		
		log += "\n\n" + new Timestamp(date.getTime());
		
		if(!engineNotification.localStatus.equals("false")){
			//If the rule not failed, update table
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				log += "\nMySQL JDBC Driver not found";
				log += e;
			}

			try {
				cc = DriverManager.getConnection(props.getProperty("DB_URL"),
						props.getProperty("DB_USER"), props.getProperty("DB_PASS"));
				Statement ss = (Statement) cc.createStatement();
				
				String checkAXOID = engineNotification.localStatus.substring(0, 4);
				
				if(checkAXOID.equals("urn:")){
					//The result is an AXOID
					query = "UPDATE " + props.getProperty("DB_TABLE") + " SET indexed = '" + (new Timestamp(date.getTime())) + "', message = 'indexed', AXOID = '" + engineNotification.localStatus + "', status = 'indexed' WHERE AXRQID = '" + engineNotification.localAXRQID + "'";
				} else {
					query = "UPDATE " + props.getProperty("DB_TABLE") + " SET message = '" + engineNotification.localStatus + "', status = 'error' WHERE AXRQID = '" + engineNotification.localAXRQID + "'";
				}

				ss.executeUpdate(query);
				
				log += "\n\nTable updated";
				
			} catch (SQLException e) {
				log += e;
			}

			finally {
				try {
					cc.close();
				}
				catch (SQLException e) {
					 log += e;
				}
			}
		} else {
			//If the rule failed, update table
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				log += "\nMySQL JDBC Driver not found";
				log += e;
			}

			try {
				cc = DriverManager.getConnection(props.getProperty("DB_URL"),
						props.getProperty("DB_USER"), props.getProperty("DB_PASS"));
				Statement ss = (Statement) cc.createStatement();

				query = "UPDATE " + props.getProperty("DB_TABLE") + " SET status = 'error' WHERE AXRQID = '" + engineNotification.localAXRQID + "'";

				ss.executeUpdate(query);
				
			} catch (SQLException e) {
				log += e;
			}

			finally {
				try {
					cc.close();
				}
				catch (SQLException e) {
					 log += e;
				}
			}
			log += "\n\nRule failed";
		}
		


		String path = props.getProperty("LOG_FILE_PATH");

		try {
			// Change file extension to be recognized
			File file = new File(path + "eclap_soap_log.txt");

			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fout = new FileWriter(path + "eclap_soap_log.txt", true);
			PrintWriter writer = new PrintWriter(fout,true);
			log += "\n\nAXRQID: " + engineNotification.localAXRQID + "\n";
			log += "Status: " + engineNotification.localStatus + "\n";
			log += "Result: " + engineNotification.localResult + "\n";
			log += "URI: " + engineNotification.localURI + "\n";
			log += "ErrorMSG: " + engineNotification.localErrorMSG
					+ "\n";
			log += "ErrorCode: " + engineNotification.localErrorCode + "\n\n";
			writer.print(log);
			writer.close();
		} catch (IOException e) {
			log += e;
		}

		return new EngineNotificationResponse();
	}

}
