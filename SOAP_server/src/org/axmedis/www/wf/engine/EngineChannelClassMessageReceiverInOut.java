
/**
 * EngineChannelClassMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
        package org.axmedis.www.wf.engine;

        /**
        *  EngineChannelClassMessageReceiverInOut message receiver
        */

        public class EngineChannelClassMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        EngineChannelClassSkeleton skel = (EngineChannelClassSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("wFProcess".equals(methodName)){
                
                org.axmedis.www.wf.engine.WFProcessResponse wFProcessResponse1 = null;
	                        org.axmedis.www.wf.engine.WFProcess wrappedParam =
                                                             (org.axmedis.www.wf.engine.WFProcess)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    org.axmedis.www.wf.engine.WFProcess.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               wFProcessResponse1 =
                                                   
                                                   
                                                         skel.wFProcess(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), wFProcessResponse1, false, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/",
                                                    "wFProcess"));
                                    } else 

            if("engineNotification".equals(methodName)){
                
                org.axmedis.www.wf.engine.EngineNotificationResponse engineNotificationResponse3 = null;
	                        org.axmedis.www.wf.engine.EngineNotification wrappedParam =
                                                             (org.axmedis.www.wf.engine.EngineNotification)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    org.axmedis.www.wf.engine.EngineNotification.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               engineNotificationResponse3 =
                                                   
                                                   
                                                         skel.engineNotification(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), engineNotificationResponse3, false, new javax.xml.namespace.QName("http://www.axmedis.org/WF/Engine/",
                                                    "engineNotification"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(org.axmedis.www.wf.engine.WFProcess param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.axmedis.www.wf.engine.WFProcess.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(org.axmedis.www.wf.engine.WFProcessResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.axmedis.www.wf.engine.WFProcessResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(org.axmedis.www.wf.engine.EngineNotification param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.axmedis.www.wf.engine.EngineNotification.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(org.axmedis.www.wf.engine.EngineNotificationResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(org.axmedis.www.wf.engine.EngineNotificationResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.axmedis.www.wf.engine.WFProcessResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(org.axmedis.www.wf.engine.WFProcessResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private org.axmedis.www.wf.engine.WFProcessResponse wrapWFProcess(){
                                org.axmedis.www.wf.engine.WFProcessResponse wrappedElement = new org.axmedis.www.wf.engine.WFProcessResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, org.axmedis.www.wf.engine.EngineNotificationResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(org.axmedis.www.wf.engine.EngineNotificationResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private org.axmedis.www.wf.engine.EngineNotificationResponse wrapEngineNotification(){
                                org.axmedis.www.wf.engine.EngineNotificationResponse wrappedElement = new org.axmedis.www.wf.engine.EngineNotificationResponse();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (org.axmedis.www.wf.engine.WFProcess.class.equals(type)){
                
                           return org.axmedis.www.wf.engine.WFProcess.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (org.axmedis.www.wf.engine.WFProcessResponse.class.equals(type)){
                
                           return org.axmedis.www.wf.engine.WFProcessResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (org.axmedis.www.wf.engine.EngineNotification.class.equals(type)){
                
                           return org.axmedis.www.wf.engine.EngineNotification.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (org.axmedis.www.wf.engine.EngineNotificationResponse.class.equals(type)){
                
                           return org.axmedis.www.wf.engine.EngineNotificationResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    