-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Gen 24, 2014 alle 00:12
-- Versione del server: 5.5.35-0ubuntu0.12.10.1
-- Versione PHP: 5.4.6-1ubuntu1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `moodle`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `last_update`
--

CREATE TABLE IF NOT EXISTS `last_update` (
  `course_id` bigint(20) NOT NULL,
  `indexed` timestamp NULL DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `processed` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `AXRQID` varchar(40) DEFAULT NULL,
  `AXOID` varchar(200) DEFAULT NULL,
  `message` text,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`course_id`),
  UNIQUE KEY `course_id` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Struttura della tabella `mdl_course_last_modules`
--

CREATE TABLE IF NOT EXISTS `mdl_course_last_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` bigint(20) NOT NULL,
  `modules` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
