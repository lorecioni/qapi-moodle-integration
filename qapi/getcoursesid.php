<?php

class GetCoursesId implements Rest_Resource {

	public function __construct() {
	}

	public function lookup($elems) {
		
		global $CFG;
		
		$content = '';
		$ids = get_records_sql('SELECT id FROM mdl_course');
		
		foreach ($ids as $id){
			$content .= $id->id.',';
		}

		return $content;

	}

}

?>