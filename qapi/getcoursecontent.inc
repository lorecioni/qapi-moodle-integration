<?php

class GetCourseContent implements Rest_Resource {
	
    public function __construct() {
    }
    
    public function lookup($elems) {

        $id = urldecode($elems[1]);

        if (! $course = get_record("course", "id", $id)) {

            error("Error finding the course");
        }

        if (! $sections = get_all_sections($id)) {

            error("Error finding or creating section structures for this course");
        }

        global $CFG, $ME;
        
        require_once ("../config.php");
        require_once ("../course/lib.php");


        // Loads the course module information. Alert! Writes the stuff back to the parameters...
        get_all_mods($course->id, $mods, $modnames, $modnamesplural, $modnamesused);

        // Switching the $ME to mimic the course view page when the header is created, to get the body class and
        // id to be mimic the course view page. Switching back after the header to avoid possible voodoo oddities.
        $OLD_ME = $ME;
        $ME = $CFG->wwwroot . "/course/view.php";

        ob_start();


        echo '<script type="text/javascript"> function printMoodleFrame(strid, strmo, pathroot){ window.scroll(0,0); $("#player-bar").remove(); $("iframe").remove(); $("<iframe name=moodleframe id=moodleframe width=100% height=600px src="+pathroot+"/mod/"+strmo+"/view.php?id="+strid+" ></iframe>").appendTo("#wrapper-player"); framecontrol(); lockcontrol(strid);}</script>';

        //get course from topics or weeks
        $sql = <<<END
			SELECT
			cou.format as form
			FROM {$CFG->prefix}course cou
			WHERE cou.id = '$elems[1]'
END;

        $result = get_records_sql($sql);

        foreach ($result as $form => $cour) {
            $topics=$cour->form;
        }


        //display different output if the course is organized weekly or by topics
        if($topics=='topics') {


            $topic = optional_param('topic', -1, PARAM_INT);

            // Bounds for block widths
            $lmin = (empty($THEME->block_l_min_width)) ? 100 : $THEME->block_l_min_width;
            $lmax = (empty($THEME->block_l_max_width)) ? 210 : $THEME->block_l_max_width;
            $rmin = (empty($THEME->block_r_min_width)) ? 100 : $THEME->block_r_min_width;
            $rmax = (empty($THEME->block_r_max_width)) ? 210 : $THEME->block_r_max_width;

            define('BLOCK_L_MIN_WIDTH', $lmin);
            define('BLOCK_L_MAX_WIDTH', $lmax);
            define('BLOCK_R_MIN_WIDTH', $rmin);
            define('BLOCK_R_MAX_WIDTH', $rmax);

            $preferred_width_left  = bounded_number(BLOCK_L_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_LEFT]),
                    BLOCK_L_MAX_WIDTH);
            $preferred_width_right = bounded_number(BLOCK_R_MIN_WIDTH, blocks_preferred_width($pageblocks[BLOCK_POS_RIGHT]),
                    BLOCK_R_MAX_WIDTH);

            if ($topic != -1) {
                $displaysection = course_set_display($course->id, $topic);
            } else {
                if (isset($USER->display[$course->id])) {       // for admins, mostly
                    $displaysection = $USER->display[$course->id];
                } else {
                    $displaysection = course_set_display($course->id, 0);
                }
            }

            $context = get_context_instance(CONTEXT_COURSE, $course->id);

            if (($marker >=0) && has_capability('moodle/course:setcurrentsection', $context) && confirm_sesskey()) {
                $course->marker = $marker;
                if (! set_field("course", "marker", $marker, "id", $course->id)) {
                    error("Could not mark that topic for this course");
                }
            }


            $stradd           = get_string('add');
            $stractivities    = get_string('activities');
            $strshowalltopics = get_string('showalltopics');
            $strtopic         = get_string('topic');
            $strgroups        = get_string('groups');
            $strgroupmy       = get_string('groupmy');
            $context = get_context_instance(CONTEXT_COURSE, $course->id);

/// Layout the whole page as three big columns (was, id="layout-table")
            echo '<br/><div>';

            echo '<div id="middle-column">'. skip_main_destination();

            echo "<ul>\n";


            /// Print Section 0 with general activities

            $section = 0;
            $thissection = $sections[$section];

            if ($thissection->summary or $thissection->sequence or isediting($course->id)) {

                echo '<div>';

                echo '<div>';
                $summaryformatoptions->noclean = true;
                echo format_text($thissection->summary, FORMAT_HTML, $summaryformatoptions);

                echo '</div>';

                $this->print_section($course, $thissection, $mods, $modnamesused);


                echo '</div>';
                echo "</li>\n";
            }


/// Now all the normal modules by week
/// Everything below uses "section" terminology - each "section" is a week.

            $timenow = time();
            $section = 1;
            $sectionmenu = array();

            $content[] .= ob_get_contents();

            ob_clean();

            while ($section <= $course->numsections) {

                if (!empty($sections[$section])) {
                    $thissection = $sections[$section];

                } else {
                    unset($thissection);
                    $thissection->course = $course->id;   // Create a new section structure
                    $thissection->section = $section;
                    $thissection->summary = '';
                    $thissection->visible = 1;
                    if (!$thissection->id = insert_record('course_sections', $thissection)) {
                        notify('Error inserting new topic!');
                    }
                }




                $showsection = (has_capability('moodle/course:viewhiddensections', $context) or $thissection->visible or !$course->hiddensections);

                if (!empty($displaysection) and $displaysection != $section) {
                    if ($showsection) {
                        $strsummary = strip_tags(format_string($thissection->summary,true));
                        if (strlen($strsummary) < 57) {
                            $strsummary = ' - '.$strsummary;
                        } else {
                            $strsummary = ' - '.substr($strsummary, 0, 60).'...';
                        }
                        $sectionmenu['topic='.$section] = s($section.$strsummary);
                    }
                    $section++;
                    continue;
                }

                if ($showsection) {

                    $currenttopic = ($course->marker == $section);

                    $currenttext = '';
                    if (!$thissection->visible) {
                        $sectionstyle = ' hidden';
                    } else if ($currenttopic) {
                        $sectionstyle = ' current';
                        $currenttext = get_accesshide(get_string('currenttopic','access'));
                    } else {
                        $sectionstyle = '';
                    }
                    echo '<li id="section-'.$section.'" >'; //'<div class="left side">&nbsp;</div>';
                    // Note, 'right side' is BEFORE content.
                    echo '<div>';


                    if (!has_capability('moodle/course:viewhiddensections', $context) and !$thissection->visible) {   // Hidden for students
                        echo get_string('notavailable');
                    } else {
                        echo '<div>';
                        $summaryformatoptions->noclean = true;
                        echo format_text($thissection->summary, FORMAT_HTML, $summaryformatoptions);

                        echo '</div>';

                        $this->print_section($course, $thissection, $mods, $modnamesused,$elems[2],$elems[1]);  //modificata rispetto a quella presente nella libreria moodle

                    }

                    echo '</div>';
                    echo "</li>\n";
                }


                $content[] .= ob_get_contents();

                ob_clean();
                $section++;
                $weekdate = $nextweekdate;
            }
            echo "</ul>\n";

            if (!empty($sectionmenu)) {
                echo '<di>';
                echo popup_form($CFG->wwwroot.'/course/view.php?id='.$course->id.'&amp;', $sectionmenu,
                'sectionmenu', '', get_string('jumpto'), '', '', true);
                echo '</div>';
            }

            echo '</div>';

            echo '</div>';
            echo '<div class="clearer"></div>';


            ob_end_clean();

            // Back to the real page $ME
            $ME = $OLD_ME;


            return $content;


        }else {
            $week = optional_param('week', -1, PARAM_INT);

            if ($week != -1) {
                $displaysection = course_set_display($course->id, $week);
            } else {
                if (isset($USER->display[$course->id])) {
                    $displaysection = $USER->display[$course->id];
                } else {
                    $displaysection = course_set_display($course->id, 0);
                }
            }


            $stradd          = get_string('add');
            $stractivities   = get_string('activities');
            $strshowallweeks = get_string('showallweeks');
            $strweek         = get_string('week');

            $context = get_context_instance(CONTEXT_COURSE, $course->id);

/// Layout the whole page as three big columns 
            echo '<br/><div>';

/// Start main column
            echo '<div>'. skip_main_destination();


            // Note, an ordered list would confuse - "1" could be the clipboard or summary.
            echo "<ul>\n";


/// Print Section 0 with general activities

            $section = 0;
            $thissection = $sections[$section];

            if ($thissection->summary or $thissection->sequence or isediting($course->id)) {

                // Note, no need for a 'left side' cell or DIV.
                // Note, 'right side' is BEFORE content.

                echo '<div>';

                echo '<div>';
                $summaryformatoptions->noclean = true;
                echo format_text($thissection->summary, FORMAT_HTML, $summaryformatoptions);


                echo '</div>';

                $this->print_section($course, $thissection, $mods, $modnamesused);


                echo '</div>';
                echo "</li>\n";
            }


/// Now all the normal modules by week
/// Everything below uses "section" terminology - each "section" is a week.

            $timenow = time();
            $weekdate = $course->startdate;    // this should be 0:00 Monday of that week
            $weekdate += 7200;                 // Add two hours to avoid possible DST problems
            $section = 1;
            $sectionmenu = array();
            $weekofseconds = 604800;
            $course->enddate = $course->startdate + ($weekofseconds * $course->numsections);

            $strftimedateshort = ' '.get_string('strftimedateshort');

            $content[] .= ob_get_contents();

            ob_clean();


            while ($weekdate < $course->enddate) {

                $nextweekdate = $weekdate + ($weekofseconds);
                $weekday = userdate($weekdate, $strftimedateshort);
                $endweekday = userdate($weekdate+518400, $strftimedateshort);

                if (!empty($sections[$section])) {
                    $thissection = $sections[$section];

                } else {
                    unset($thissection);
                    $thissection->course = $course->id;   // Create a new week structure
                    $thissection->section = $section;
                    $thissection->summary = '';
                    $thissection->visible = 1;
                    if (!$thissection->id = insert_record('course_sections', $thissection)) {
                        notify('Error inserting new week!');
                    }
                }

                $showsection = (has_capability('moodle/course:viewhiddensections', $context) or $thissection->visible or !$course->hiddensections);
                if (!empty($displaysection) and $displaysection != $section) {  // Check this week is visible
                    if ($showsection) {
                        $sectionmenu['week='.$section] = s("$strweek $section |     $weekday - $endweekday");
                    }
                    $section++;
                    $weekdate = $nextweekdate;
                    continue;
                }
                if ($showsection) {
                    $currentweek = (($weekdate <= $timenow) && ($timenow < $nextweekdate));
                    $currenttext = '';
                    if (!$thissection->visible) {
                        $sectionstyle = ' hidden';
                    } else if ($currentweek) {
                        $sectionstyle = ' current';
                        $currenttext = get_accesshide(get_string('currentweek','access'));
                    } else {
                        $sectionstyle = '';
                    }
                    echo '<li id="section-'.$section.'" >';

                    echo '<div>';

                    echo '</div>';

                    $weekperiod = $weekday.' - '.$endweekday;

                    echo '<div>';
                    if (!has_capability('moodle/course:viewhiddensections', $context) and !$thissection->visible) {   // Hidden for students
                        print_heading($currenttext.$weekperiod.' ('.get_string('notavailable').')', null, 3, 'weekdates');

                    } else {
                        print_heading($currenttext.$weekperiod, null, 3, 'weekdates');

                        echo '<div>';
                        $summaryformatoptions->noclean = true;
                        echo format_text($thissection->summary, FORMAT_HTML, $summaryformatoptions);

                        echo '</div>';

                        $this->print_section($course, $thissection, $mods, $modnamesused, $elems[2],$elems[1]);  //modificata rispetto a quella presente nella libreria moodle

                    }

                    echo '</div>';
                    echo "</li>\n";
                }


                $content[] .= ob_get_contents();

                ob_clean();
                $section++;
                $weekdate = $nextweekdate;
            }
            echo "</ul>\n";

            if (!empty($sectionmenu)) {
                echo '<div>';
                echo popup_form($CFG->wwwroot.'/course/view.php?id='.$course->id.'&amp;', $sectionmenu,
                'sectionmenu', '', get_string('jumpto'), '', '', true);
                echo '</div>';
            }

            echo '</div>';

            echo '</div>';
            echo '<div class="clearer"></div>';


            ob_end_clean();

            // Back to the real page $ME
            $ME = $OLD_ME;


            return $content;

        }
    }

    /**
     *  Print a section of a course
     */

    function print_section($course, $section, $mods, $modnamesused, $drupalid, $courseid, $absolute=false, $width="100%") {

/// Prints a section full of activity modules
        global $CFG, $USER;
        
        static $initialised;
        static $groupbuttons;
        static $groupbuttonslink;
        static $strmovehere;
        static $strmovefull;
        static $strunreadpostsone;
        static $usetracking;
        static $groupings;


        if (!isset($initialised)) {
            $groupbuttons     = ($course->groupmode or (!$course->groupmodeforce));
            $groupbuttonslink = (!$course->groupmodeforce);
            $isediting        = isediting($course->id);
            $ismoving         = $isediting && ismoving($course->id);
            if ($ismoving) {
                $strmovehere  = get_string("movehere");
                $strmovefull  = strip_tags(get_string("movefull", "", "'$USER->activitycopyname'"));
            }
            include_once($CFG->dirroot.'/mod/forum/lib.php');
            if ($usetracking = forum_tp_can_track_forums()) {
                $strunreadpostsone = get_string('unreadpostsone', 'forum');
            }
            $initialised = true;
        }

        $labelformatoptions = new object();
        $labelformatoptions->noclean = true;

/// Casting $course->modinfo to string prevents one notice when the field is null
        $modinfo = get_fast_modinfo($course);


        //Acccessibility: replace table with list <ul>, but don't output empty list.
        if (!empty($section->sequence)) {


            echo "<ul class=\"section img-text\">\n";
            $sectionmods = explode(",", $section->sequence);

            foreach ($sectionmods as $modnumber) {
                if (empty($mods[$modnumber])) {
                    continue;
                }

                $mod = $mods[$modnumber];

                echo '<li title="'.$mod->modname.'" class="activity '.$mod->modname.'" id="module-'.$modnumber.'">';  // Unique ID

                $extra = '';
                if (!empty($modinfo->cms[$modnumber]->extra)) {
                    $extra = $modinfo->cms[$modnumber]->extra;
                }

                if ($mod->modname == "label") {
                    echo "<span class=\"";
                    if (!$mod->visible) {
                        echo 'dimmed_text';
                    } else {
                        echo 'label';
                    }
                    echo '">';
                    echo format_text($extra, FORMAT_HTML, $labelformatoptions);
                    echo "</span>";
                    if (!empty($CFG->enablegroupings) && !empty($mod->groupingid) && has_capability('moodle/course:managegroups', get_context_instance(CONTEXT_COURSE, $course->id))) {
                        if (!isset($groupings)) {
                            $groupings = groups_get_all_groupings($course->id);
                        }
                        echo " <span class=\"groupinglabel\">(".format_string($groupings[$mod->groupingid]->name).')</span>';
                    }

                } else { // Normal activity
                    $instancename = format_string($modinfo->cms[$modnumber]->name, true,  $course->id);

                    if (!empty($modinfo->cms[$modnumber]->icon)) {
                        $icon = "$CFG->pixpath/".$modinfo->cms[$modnumber]->icon;
                    } else {
                        $icon = "$CFG->modpixpath/$mod->modname/icon.gif";
                    }

                    //Accessibility: for files get description via icon.
                    $altname = '';
                    // Avoid unnecessary duplication.
                    if (false!==stripos($instancename, $altname)) {
                        $altname = '';
                    }
                    // File type after name, for alphabetic lists (screen reader).
                    if ($altname) {
                        $altname = get_accesshide(' '.$altname);
                    }

                    $linkcss = $mod->visible ? "" : " class=\"dimmed\" ";

                    if($mod->modname!='resource') {
                        
                        //controllo dei lock in accesso
                        $queryDrupal=get_record_sql("SELECT * FROM drupal.users WHERE uid='".$drupalid."'");
                        $queryMoodle=get_record_sql("SELECT * FROM moodle.mdl_user WHERE username ='".$queryDrupal->name."'");
                        $uid=$queryMoodle->id;
                        $lockmod=is_locked_access($mod,$uid,$course,$drupalid);
                        if (in_array("closed",$lockmod)){
                            $disLock='';
                        }
                        else {
                            $disLock='style="display:none;"';
                        }
                        $iconLock="$CFG->pixpath/lucchetto16x16.png";
                        $imgicon='<img src="'.$iconLock.'" id="icon-lock-'.$modnumber.'" '.$disLock.' /> ';
                        echo '<div id="unread"> <a id="ref" href=javascript:printMoodleFrame('.$mod->id.',"'.$mod->modname.'","'.$CFG->wwwroot.'")  >'.'<img src="'.$icon.'" id="icon" class="activityicon" alt="" /> '.$instancename.$altname.'  '.$imgicon;

                        echo '</a></div>';

                    }else {

                        //TODO add here other resource, link web, static text page, etc....
                        if (! $resource = get_record('resource', 'id', $mod->instance)) {
                            error('Resource ID was incorrect');
                        }

                        //   TODO agire su href per modificare i link ai file
                        if($resource->type == 'axmedismedia') {
						$icon = "$CFG->wwwroot/mod/resource/type/axmedismedia/axmedismedia.gif"; //posizionare l'icona sulla cartella

                            echo '<img src="'.$icon.'"'.
                                    ' class="activityicon" alt="'.$mod->modfullname.'" />'.
                                    ' <a title="'.$mod->modfullname.'" '.$extra.
                                    ' href="?q=user/'.$drupalid.'/courses/'.$courseid.'/content/resource/'.$resource->id.'/'.$resource->reference.'">'.
                                    $instancename.'</a><br/>';
                        }else if($resource->type == 'elementcollection') {
								
								echo '<img src="'.$icon.'"'.
                                    ' class="activityicon" alt="'.$mod->modfullname.'" />'.
                                    ' <a title="'.$mod->modfullname.'" '.$extra.
                                    ' href="?q=user/'.$drupalid.'/courses/'.$courseid.'/content/resource/'.$resource->id.'/'.$resource->alltext.'">'.
                                    $instancename.'</a><br/>';
								
						}else {

                            echo '<div id="unread"><a href=javascript:printMoodleFrame('.$mod->id.',"'.$mod->modname.'","'.$CFG->wwwroot.'") >'.'<img src="'.$icon.'" class="activityicon" id="icon" alt="" /> '.$instancename.$altname;

                        }

                    }

                }

                echo "</li>\n";
            }

        }

    }

} 


function is_locked_access($module,$uid,$course,$drupalid){
        global $CFG;  //It there an alternative to global $course?
   	static $currentcourse, $mods; // Hopefully this should reduce the number of queries
        $userid = $uid;
        
        $courseid = $course->id;

	if (!isset($module)) {   // limit Notice: Trying to get property of non-object in Header
            return false;
	}

	if (!isset($mods) or !($currentcourse == $courseid)) {     // The idea is, you should only need to download a list of modules in a course once
   		$mods = get_course_mods($courseid);
   		$currentcourse = $courseid;
   	}

   	if ($modlocks = get_records("course_module_locks", "moduleid", $module->id)) {
   	  foreach($modlocks as $modlock) {
   		$lockid = $modlock->lockid;
		if ($lockid == $modlock->moduleid) {

                    //locked on access
                    if ($modlock->requirement == 'A') {
                        if (record_exists("log", "userid", $userid, "cmid", $modlock->moduleid)) {
                                $locks[$modlock->moduleid]  = "open";
                        } else {
                                $locks[$modlock->moduleid] = "closed";
                        }
                    }
                }

                if ($lockid != $modlock->moduleid) {
                    
                    //locked on access
                    if ($modlock->requirement == 'A') {
                        if (record_exists("log", "userid", $uid, "module", $mods[$lockid]->modname , "info" , $mods[$lockid]->instance)) {
                                $locks[$modlock->lockid]  = "open";
                        } else {
                                $locks[$modlock->lockid] = "closed";
                        }
                    }
                }
            }
        }
	if (isset($locks)) {
		return $locks;
	} else { // No locks on module
   		return false;
	}
}

	

?>
