<?php

/**
 * Quick API: a RESTful web service module version information.
 *
 * @ First author Chris Johnson 
 
 **/

$plugin->version  = 2008122601;  // The current module version (Date: YYYYMMDDXX)
$plugin->requires = 2007101507;  // Requires this Moodle version

?>
