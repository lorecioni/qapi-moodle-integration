<?php

class GetCourseContentXml implements Rest_Resource {

	public function __construct() {
	}

	public function lookup($elems) {

		header ("Content-type: text/xml");

		$id = urldecode($elems[1]);

		if (! $course = get_record("course", "id", $id)) {
			delete_records('course_last_modules', 'course_id', $id);
			error("Error finding the course");
		}

		if (! $sections = get_all_sections($id)) {

			error("Error finding or creating section structures for this course");
		}

		// Loads the course module information. Alert! Writes the stuff back to the parameters.
		get_all_mods($course->id, $mods, $modnames, $modnamesplural, $modnamesused);

		ob_start();

		$content = '<?xml version="1.0" encoding="ISO-8859-1"?>';
		$content = $content.'<course>';

		//Salva le informazioni sul corso
		$course_id = $course->id;
		$course_title = $course->fullname;
		$course_desc = $course->summary;

		$date = $course->timecreated;
		$creation_date = date('Y-m-d H:i:s',$date);
		
		$content .= '<id>'.$course_id.'</id>';
		$content .= '<title>'.$course_title.'</title>';
		$content .= '<description>'.htmlspecialchars($course_desc).'</description>';
		$content .= '<creation_date>'.$creation_date.'</creation_date>';	

		//Docenti del corso

		$teachers = array();
		
		$context = get_context_instance(CONTEXT_COURSE, $course_id);
		$managerroles = split(',', '1,2,3');
		$canseehidden = has_capability('moodle/role:viewhiddenassigns', $context);

		$rusers = get_role_users($managerroles, $context,
				true, '', 'r.sortorder ASC, u.lastname ASC', $canseehidden);
		
		if (is_array($rusers) && count($rusers)) {
			foreach ($rusers as $teacher) {
				$teachers[] = $teacher;
			}
		}

		$content .= '<teachers>';
		
		foreach($teachers as $teacher){
			$content .= '<teacher>'.$teacher->firstname.' '.$teacher->lastname.'</teacher>';
			if($teacher->roleid == 2){
				//Course creator and owner
				$owner_usrname = $teacher->username;
			}
		}
		
		$content .= '</teachers>';
		
		//Getting the affiliation group for the owner of the course from drupal db
		
		include ('eclap_config.php');
		$result_affl = mysql_query("SELECT affiliation, u.uid FROM med_info m, users u WHERE m.uid = u.uid AND u.name = '".$owner_usrname."'") or trigger_error(mysql_error());
		while($data = mysql_fetch_row($result_affl)){
			if($data[0] != ''){
				$affiliation = $data[0];
				$uid = $data[1];				
			} else {
				$affiliation = 'NONE';
			}
		}
		closeConnection();
		
		$content .= '<course_creator>';
		$content .= '<userid>'.$uid.'</userid>';
		$content .= '<affiliation>'.$affiliation.'</affiliation>';
		$content .= '</course_creator>';

		//Ultima modifica alla struttura del corso
		$last_modified = $course->timemodified;
		
		//Carica le sezioni del corso

		$content .= '<sections>';

		
		foreach($sections as $section){
			$content .= '<section>';
			$summary = $section->summary;
			 
			if($summary == null){
				$summary = ' ';
			}
			$content .= '<number>'.$section->section.'</number>';
			$content .= '<summary>'.htmlspecialchars($summary).'</summary>';
			 
			//Carico i moduli della sezione corrente
			$content .= '<modules>';
			 
			if (!empty($section->sequence)) {

				$sectionmods = explode(",", $section->sequence);

				foreach ($sectionmods as $modnumber) {
					if (empty($mods[$modnumber])) {
						continue;
					}

					$mod = $mods[$modnumber];
					 
					$content .= '<module>';
					$content .= '<type>'.$mod->modname.'</type>';

					//Modulo risorsa
					if($mod->modname=='resource') {
						$resource = get_record('resource', 'id', $mod->instance);
						$content .= '<name>'.$resource->name.'</name>';
						$content .= '<type>'.$resource->type.'</type>';
						$content .= '<filename>'.htmlspecialchars($resource->reference).'</filename>';
						$content .= '<description>'.htmlspecialchars($resource->summary).'</description>';
						$res_content = $resource->alltext;
						if($res_content == null){
							$res_content = ' ';
						}
						$content .= '<text>'.htmlspecialchars($res_content).'</text>';
						
						if($last_modified < $resource->timemodified){
							$last_modified = $resource->timemodified;
						}
					}

					//Modulo forum
					if($mod->modname=='forum') {
						$resource = get_record('forum', 'id', $mod->instance);
						$content .= '<name>'.$resource->name.'</name>';
						$content .= '<description>'.htmlspecialchars($resource->intro).'</description>';
						
						if($last_modified < $resource->timemodified){
							$last_modified = $resource->timemodified;
						}
						
						$discussions = get_records('forum_discussions', 'forum', $mod->instance);
						 
						//Preleva le discussioni del forum
						$content .= '<discussions>';
						foreach($discussions as $discuss){
							$content .= '<discussion>';
							$content .= '<title>'.$discuss->name.'</title>';
							
							if($last_modified < $discuss->timemodified){
								$last_modified = $discuss->timemodified;
							}
							
							$content .= '<posts>';
							//Preleva i post della discussione corrente
							$posts = get_records('forum_posts', 'discussion', $discuss->id);
							foreach($posts as $post){
								$content .= '<post>';
								$content .= '<subject>'.htmlspecialchars($post->subject).'</subject>';
								$content .= '<message>'.htmlspecialchars($post->message).'</message>';

								$author = get_record('user', 'id', $post->userid);
								 
								$content .= '<author>'.$author->firstname.' '.$author->lastname.'</author>';
								$content .= '</post>';
								
								if($last_modified < $post->modified){
									$last_modified = $post->modified;
								}
							}
							$content .= '</posts>';
							$content .= '</discussion>';
						}
						$content .= '</discussions>';
					}

					//Modulo label
					if($mod->modname=='label') {
						$resource = get_record('label', 'id', $mod->instance);
						$content .= '<name>'.$resource->name.'</name>';
						$content .= '<content>'.htmlspecialchars($resource->content).'</content>';
						
						if($last_modified < $resource->timemodified){
							$last_modified = $resource->timemodified;								
						}
					}

					//Modulo lesson
					if($mod->modname=='lesson') {
						$resource = get_record('lesson', 'id', $mod->instance);
						$content .= '<name>'.$resource->name.'</name>';
						
						if($last_modified < $resource->timemodified){
							$last_modified = $resource->timemodified;
							
						}
					}

					$content .= '</module>';
				}
			}
			 
			$content .= '</modules></section>';
		}
		
		$content .= '</sections>';
		
		$content .= '<events>';
		
		$events = get_records('event', 'courseid', $course_id);
		
		foreach ($events as $event){
			$content .= '<event>';
			$content .= '<name>'.$event->name.'</name>';
			$content .= '<description>'.htmlspecialchars($event->description).'</description>';
			$content .= '<start_time>'.date('Y-m-d h:i:s',$event->timestart).'</start_time>';
			$author = get_record('user', 'id', $event->userid);
			$content .= '<created_by>'.$author->firstname.' '.$author->lastname.'</created_by>';
			$content .= '</event>';
			
			if($last_modified < $event->timemodified){
				$last_modified = $event->timemodified;
			}
		}
		
		$content .= '</events>';
		
		//Recupera gli id delle risorse presenti alla richiesta precedente della pagina
		$result = get_record('course_last_modules', 'course_id', $course_id);
		
		$inset = true;
		
		$current_modules = array();
			
		foreach ($mods as $module){
			$current_modules[] = $module->id;
		}
		
		$new_modules = '';
			
		foreach ($current_modules as $cur_mod){
			$new_modules .= $cur_mod.' ';
		}
			
		$new_modules .= '0';
		
		if($result->modules != '') {
			//Carica gli id dei moduli che erano presenti all'ultima chiamata
			$pieces = explode(" ", $result->modules);
			
			$old_modules = array();
			
			foreach ($pieces as $piece){
				$old_modules[] = $piece;
				if(!in_array($piece, $current_modules) && $piece != '0'){
					$inset = false;
				}
			}
			
			set_field('course_last_modules', 'modules', $new_modules, 'course_id', $course_id);
		
		} else {
			$obj = new stdClass();
			$obj->course_id = $course_id;
			$obj->modules = $new_modules;
			insert_record('course_last_modules', $obj);
		}
		
		
		
		if($inset == false){
			$last_modified = date("Y-m-d H:i:s"); 			
			$course->timemodified = strtotime($last_modified);
			update_record("course",$course);
		}
		else {
			$last_modified = date('Y-m-d H:i:s',$last_modified);
		}
		
		$content .= '<last_modified>'.$last_modified.'</last_modified>';
		
		$content = $content.'</course>';

		return $content;
	}
}
?>
