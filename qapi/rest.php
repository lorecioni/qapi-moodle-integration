<?php


define('QAPI_CLIENT_TAB', 'qapi_client'); // database table name, duplicated in index.php

interface Rest_Resource {
    public function lookup($path_elements);
}

if (!file_exists('../config.php')) {
    header('HTTP/1.1 403 Forbidden');
    header('Status: 403 Forbidden');
    print json_encode(array('error' => 'Moodle not properly installed; config.php missing.'));
    exit;
}

require_once('../config.php');
require_once($CFG->dirroot .'/course/lib.php');

if (empty($SITE)) {
    redirect($CFG->wwwroot .'/'. $CFG->admin .'/index.php');
}

// validate request came from a trusted client system
// if (!trusted_client()) {
//     header('HTTP/1.1 403 Forbidden');
//     header('Status: 403 Forbidden');
//     print json_encode(array('error' => 'authentication error for '.$_SERVER['REMOTE_ADDR']));
//     exit;
// }


// get resource path elements
$elements = explode('/', htmlspecialchars($_SERVER['PATH_INFO'], ENT_QUOTES));  // todo or should this be urldecode()?
array_shift($elements); // elements[0] is always empty

// interface Rest_Resource implementations:
// instantiate proper class for type of request

switch ($elements[0]) {

    case 'getcourses':
        include_once "getcourses.inc";
        $resource = new GetCourses;
        break;

    case 'getcategories':
        include_once "getcategories.inc";
        $resource = new GetCategories;
        break;

    case 'getcourseteachers':
        include_once "getcourseteachers.inc";
        $resource = new GetCourseTeachers;
        break;

    case 'content':
        include_once "getcoursecontent.inc";
        $resource = new GetCourseContent;
        break;
        
    case 'coursexml':
    	include_once "getcoursecontentxml.inc";
    	$resource = new GetCourseContentXml;
    	break;

    case 'courseid':
    	include_once "getcoursesid.php";
    	$resource = new GetCoursesId;
    	break;
    	
    case 'isteacher':
        include_once "isteacher.inc";
        $resource = new IsTeacher;
        break;

    case 'insertlog':
        include_once "inslog.inc";
        $resource = new InsLog;
        break;

    case 'debug':
        break;

    default:
        $resource = new Error_Msg_Course;
}

$output = $resource->lookup($elements);

/* DEBUG */
if (0) {
    echo "<pre>";
    echo "input:\n";
    print_r($elements);
    echo "output:\n";
    print_r($output);
    echo "</pre>";
}
/* END DEBUG */

if($elements[0] != 'coursexml' && $elements[0] != 'courseid'){
	print json_encode($output);
}
else {
	//Se è richiesta solo la struttura XML, l'output viene visualizzato non codificato in json.
	echo $output;
}
exit;


class Error_Msg_Course {
    function lookup($elements) {
        $elements['error'] = 'unrecognized course request';
        return $elements;
        return array('error' => 'unrecognized course request');
    }
}




/**
 * validate a remote IP as a trusted site.
 */
function trusted_client() {
    global $CFG;

    $records = get_records(QAPI_CLIENT_TAB);
    $clientip = $_SERVER['REMOTE_ADDR'];

    foreach ($records as $record) {
        $trustedip = $record->clientip;
        $sharedkey = $record->sharedkey;

        if (empty($trustedip)) {
            return FALSE;
        }
        if ($clientip == $trustedip) {
            return TRUE;
        }
    }
    return FALSE;
}    
?>
